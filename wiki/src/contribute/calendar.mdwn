[[!meta title="Calendar"]]

* 2017-04-18:
  - All branches targeting Tails 2.12 *must* be merged into the
    `testing` branch by noon, CET.
  - The upcoming Tor Browser is hopefully out so we can import it.
  - Build and upload Tails 2.12 ISO image and IUKs.
  - Hopefully start testing Tails 2.12.

* 2017-04-19:
  - Finish testing Tails 2.12 by the afternoon, CET.
  - Release Tails 2.12 during late CET.

* 2017-04-19: Release 3.0~betaN — intrigeri is the RM

* 2017-05-04, 16:00 (Berlin time): CI team meeting

* 2017-05-19: Build, upload and start testing 3.0~rc1 — intrigeri and
              anonym are joint RMs

* 2017-05-20: Finish testing and release 3.0~rc1 — anonym is the RM

* 2017-06-01, 16:00 (Berlin time): CI team meeting

* 2017-06-13: Release 3.0? (Firefox 52.2) — intrigeri is the RM

* 2017-07-06, 16:00 (Berlin time): CI team meeting

* 2017-08-08: Release 3.1? (Firefox 52.3)
  - bertagaz is the RM
  - intrigeri is on-call for foundations work (fix 3.0 bugs) until
    2017-07-20.
  - anonym would like to be more or less AFK during the whole cycle.
    Possibly he takes over intrigeri's hat after 2017-07-20.

* 2017-09-07, 16:00 (Berlin time): CI team meeting

* 2017-10-03: Release 3.2? (Firefox 52.4) — anonym is the RM

* 2017-10-05, 16:00 (Berlin time): CI team meeting

* 2017-11-02, 16:00 (Berlin time): CI team meeting

* 2017-11-14: Release 3.3? (Firefox 52.5) — anonym is the RM

* 2017-12-07, 16:00 (Berlin time): CI team meeting

* 2018-01-04, 16:00 (Berlin time): CI team meeting

* 2018-01-16: Release 3.4? (Firefox 52.6)

* 2018-02-01, 16:00 (Berlin time): CI team meeting
